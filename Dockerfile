FROM debian:11

WORKDIR /app
RUN apt update
RUN apt install -y python3 python3-pil python3-pip libtesseract-dev tesseract-ocr ffmpeg libsm6 libxext6
COPY . .
RUN pip3 install -r requirements.txt
CMD ["python3", "scan.py"]