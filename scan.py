from email.mime import image
import os
from kombu import Connection, Exchange, Consumer, Queue
from dotenv import load_dotenv
from pytesseract import image_to_string
import cv2
import numpy as np
import requests

load_dotenv()
imgserver_address = os.getenv("IMGSERVER", "http://127.0.0.1:8000")

def process_message(body, message):
    # Convert bytestring to numpy array for opencv
    scan_id = message.headers.get("uuid")
    print("Scanning image:", scan_id)
    try:
        imgarray = np.frombuffer(body, dtype=np.uint8)
        img = cv2.imdecode(imgarray, flags=1)
        
        text = image_to_string(img)
        if not text:
            statuscode = -1
            print(f"Failed to get string from image. id: {scan_id}")
        else:
            statuscode = 1
            print(f"Processed message id: {scan_id}")

        send_status(statuscode, scan_id, text) 
        # Acknowledge the message
    except Exception as e:
        print(e)
    message.ack()

def send_status(statuscode, scan_id, text):
    """
    send the status of the process back to frontend 
    """
    response = requests.post(imgserver_address + "/img/scan_status/", data={"scan_id": scan_id, "statuscode": statuscode, "text": text})
    if response.status_code != 200:
        print("Failed to update status:", response.content)
        # Sometimes the text doesn't go through django's form validation, in that case we'll just do this
        response = requests.post(imgserver_address + "/img/scan_status/", data={"scan_id": scan_id, "statuscode": -1, "text": ""})

if __name__=="__main__":
    # Read config values
    qname = os.environ.get("QUEUE", "queue")
    routing_key = os.environ.get("ROUTING_KEY", "img")
    exchange_name = os.environ.get("EXCHANGE", "img_exchange")
    rabbit_address = os.environ.get("RABBIT", "amqp://127.0.0.1:8081/")

    connection = Connection(rabbit_address)
    exchange = Exchange(exchange_name, type="direct")
    queue = Queue(name=qname, exchange=exchange, routing_key=routing_key)

    with Consumer(connection, queues=queue, callbacks=[process_message],
                accept=["text/plain"]):
        while True:
            connection.drain_events()
    